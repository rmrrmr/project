from tkinter import *
from tkinter import ttk
from gen import *
from utils import *

def clear():
    if alustatud == 1:
        global kasutajamuutuja
        kasutajamuutuja = ""
        ülesandetekstvar.set(ülesandetekst + "=" + kasutajamuutuja)
    return
def vahetamark():
    global kasutajamuutuja
    if alustatud == 1:
        if kasutajamuutuja == "":
            kasutajamuutuja = "-"
        else:
            if kasutajamuutuja[0] == "-":
                kasutajamuutuja = kasutajamuutuja[1:]
            else:
                kasutajamuutuja = "-" + kasutajamuutuja
        ülesandetekstvar.set(ülesandetekst + "=" + kasutajamuutuja)
    return
def uusülesanne():
    global tase
    global ülesandetekst
    global vastus
    global kasutajamuutuja
    ülesandetekst,vastus = ülesanne(tase)
    kasutajamuutuja = ""
    ülesandetekstvar.set(ülesandetekst + "=" + kasutajamuutuja)
    return

def nupuvajutus(nupp, symbol):
    if(nupp.isdigit()):
        nupufunktsioon(nupp)
    elif(nupp.isspace()):
        if alustatud == 1:
            kontroll()
        else:
            start()
    elif(symbol == "BackSpace"):
        tagasi()
    elif(nupp == "-"):
        vahetamark()
    elif(nupp == "."):
        nupufunktsioon(nupp)
        
def nupufunktsioon(a):
    global kasutajamuutuja
    if alustatud == 1:
        kasutajamuutuja += str(a)
        ülesandetekstvar.set(ülesandetekst + "=" + kasutajamuutuja)
    return
def kontroll():
    global tase
    global loendur
    global punkte
    if alustatud == 1:
        if kasutajamuutuja == "":
            return
        if float(kasutajamuutuja)==vastus:
            loendur += 1
            punkte += tase*2
            punktidevar.set("Punkte: " + str(punkte))
            if loendur == 5:
                loendur = 0
                if tase < 6:
                    tase += 1
                tasemevar.set("Tase: " + str(tase))
            uusülesanne()
    return
def start():
    global alustatud
    global algusaeg
    global ülesandetekst
    global kasutajamuutuja
    global vastus
    global punkte
    global aeg
    if alustatud == 0:
        kasutajamuutuja = ""
        punkte = 0
        tase = 1
        punktidevar.set("Punkte: " + str(punkte))
        ülesandetekst,vastus = ülesanne(tase)
        ülesandetekstvar.set(ülesandetekst + "=" + kasutajamuutuja)
        aeg = 60
        ajavar.set("Aega järel: " + str(aeg))
        alustatud = 1
        tasemevar.set("Tase: " + str(tase))
    return
def tagasi():
    global kasutajamuutuja
    if kasutajamuutuja == "":
        return
    else:
        kasutajamuutuja = kasutajamuutuja[:(len(kasutajamuutuja)-1)]
        ülesandetekstvar.set(ülesandetekst + "=" + kasutajamuutuja)
        return
    
def uuendaajavar():
    global alustatud
    global algusaeg
    global aeg
    if alustatud == 1:
        aeg -= 1
        ajavar.set("Aega järel: " + str(aeg))
        if aeg == 0 or aeg < 0:
            messagebox.showinfo(message="Aeg on läbi!")
            alustatud = 0
            ülesandetekstvar.set("Vajuta Start nuppu")
            saveResultToFile(punkte)
    raam.after(1000,uuendaajavar)


    
#################################
#          PÕHISÄTTED           #
#################################
tervitus()

raam = Tk()
raam.title("Mini-pranglimine")
raam.geometry("400x250")
tekstifont = font.Font(family='Helvetica', size=10, weight='normal')
ülesandefont = font.Font(family='Helvetica', size=20, weight='bold')

#################################
#           MUUTUJAD            #
#################################

punkte = 0
loendur = 0
tase = 1
ülesandetekst,vastus = 0,0
kasutajamuutuja = ""
alustatud = 0
ülesandetekstvar = StringVar()
ülesandetekstvar.set("Vajuta Start nuppu")
punktidevar = StringVar()
ajavar = StringVar()
tasemevar = StringVar()

#################################
#           NUPUD               #
#################################

nupp1=ttk.Button(raam, text="1", command=lambda:nupufunktsioon(1))
nupp1.place(x=5, y=40, width=50, height=50)
nupp2=ttk.Button(raam, text="2", command=lambda:nupufunktsioon(2))
nupp2.place(x=55, y=40, width=50,height=50)
nupp2=ttk.Button(raam, text="3", command=lambda:nupufunktsioon(3))
nupp2.place(x=105, y=40, width=50, height=50)

nupp4=ttk.Button(raam, text="4", command=lambda:nupufunktsioon(4))
nupp4.place(x=5, y=90, width=50, height=50)
nupp5=ttk.Button(raam, text="5", command=lambda:nupufunktsioon(5))
nupp5.place(x=55, y=90, width=50, height=50)
nupp6=ttk.Button(raam, text="6", command=lambda:nupufunktsioon(6))
nupp6.place(x=105, y=90, width=50, height=50)

nupp7=ttk.Button(raam, text="7", command=lambda:nupufunktsioon(7))
nupp7.place(x=5, y=140, width=50,height=50)
nupp8=ttk.Button(raam, text="8", command=lambda:nupufunktsioon(8))
nupp8.place(x=55, y=140, width=50, height=50)
nupp9=ttk.Button(raam, text="9", command=lambda:nupufunktsioon(9))
nupp9.place(x=105, y=140, width=50, height=50)

nupp0=ttk.Button(raam, text="0", command=lambda:nupufunktsioon(0))
nupp0.place(x=55, y=190, width=50, height=50)

nuppesita=ttk.Button(raam,text="Esita", command=kontroll)
nuppesita.place(x=155, y=140,width=50, height=50)

nuppmark=ttk.Button(raam,text="+/-", command=vahetamark)
nuppmark.place(x=5, y=190, width=50, height=50)

nuppkoma = ttk.Button(raam,text=".", command=lambda: nupufunktsioon("."))
nuppkoma.place(x=105, y=190, width=50, height=50)

nuppstart=ttk.Button(raam, text="Start", command=start)
nuppstart.place(x=155, y=190, width=50, height=50)

nuppclear=ttk.Button(raam, text="Tühista", command=clear)
nuppclear.place(x=155, y=40, width=50, height=50)

nuppback=ttk.Button(raam, text="<-", command=tagasi)
nuppback.place(x=155, y=90, width=50, height=50)

nuppnaitatulemust = ttk.Button(raam, text = "Vaata parimaid tulemusi", command = lambda: resultWindow())
nuppnaitatulemust.place(x = 230, y = 190, width = 150, height = 50)


#################################
#           SILDID              #
#################################

ülesandesilt = ttk.Label(raam, textvariable=ülesandetekstvar,font=ülesandefont)
ülesandesilt.place(x=5,y=5)

ajasilt = ttk.Label(raam, textvariable=ajavar,font=tekstifont)
ajasilt.place(x=205,y=40)

punktidesilt = ttk.Label(raam,textvariable=punktidevar,font=tekstifont)
punktidesilt.place(x=205,y=90)

tasemesilt = ttk.Label(raam,textvariable=tasemevar,font=tekstifont)
tasemesilt.place(x=205,y=140)

#################################
uuendaajavar()


raam.bind_all('<Key>', lambda e: nupuvajutus(e.char, e.keysym))

raam.mainloop()

