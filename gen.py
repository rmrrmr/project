from random import randint
from random import uniform

def negsulud(a):
    if a<0:
        return "("+str(a)+")"
    else:
        return str(a)

    
def negmärk(a):
    if a<0:
        return str(a)
    else:
        return "+"+str(a)


def liitminelahutamine(a,b,c):
    x = randint(a,b)
    y = randint(a,b)
    while abs(x) < c:
        x = randint(a,b)
    while abs(y) < c:
        y = randint(a,b)
    tehe = str(x)+negmärk(y)
    vastus = x+y
    return (tehe,vastus)


def korrutamine(a,b,c):
    x = randint(a,b)
    y = randint(a,b)
    while abs(x)<c:
        x = randint(a,b)
    while abs(y)<c:
        y = randint(a,b)
    tehe = str(x)+"*"+negsulud(y)
    vastus = x*y
    return (tehe,vastus)


def liidakorruta(a,b,c,d):
    sulutehe,sulusumma = liitminelahutamine(-a,a,b)
    korrutaja = randint(-c,c)
    while korrutaja < d:
        korrutaja = randint(-c,c)
    tehe = "(" + sulutehe + ")" + "*" + negsulud(korrutaja)
    vastus = sulusumma*korrutaja
    return tehe,vastus

# liitmine kümnendmurdudega (ühekohaline komakoht)
def liidaKymnendmurd(lowerLevel, upperLevel):
    first = round(uniform(lowerLevel, upperLevel), 1)
    second = round(uniform(lowerLevel,upperLevel), 1)
    tehe = str(first) + " + " + str(second)
    vastus = first + second
    return(tehe, vastus)

# lahutamine kümnendmurdudega (ühekohaline komakoht)
def lahutaKymnendmurd(lowerLevel, upperLevel):
    first = round(uniform(lowerLevel, upperLevel), 1)
    second = round(uniform(lowerLevel,upperLevel), 1)
    tehe = str(first) + " - " + str(second)
    vastus = first - second
    return (tehe, vastus)



###gen2 funktsioonid:
def korrutaliida(korrutasuurus,korrutamiinimum,liidasuurus,liidamiinimum):
    x = randint(-korrutasuurus,korrutasuurus)
    y = randint(-korrutasuurus,korrutasuurus)
    while abs(x)<korrutamiinimum:
        x = randint(-korrutasuurus,korrutasuurus)
    while abs(y)<korrutamiinimum:
        y = randint(-korrutasuurus,korrutasuurus)
    z = randint(-liidasuurus,liidasuurus)
    while abs(z)<liidamiinimum:
        z = randint(-liidasuurus,liidasuurus)
    tehe = str(x) + "*" + negsulud(y) + negmärk(z)
    vastus = x*y + z
    return tehe,vastus

def liidakorruta(liidetavasuurus,liidetavamiinimum,korrutajasuurus,korrutajamiinimum):
    sulutehe,sulusumma = liitminelahutamine(liidetavasuurus*(-1),liidetavasuurus,liidetavamiinimum)
    korrutaja = randint(-korrutajasuurus,korrutajasuurus)
    while korrutaja < korrutajamiinimum:
        korrutaja = randint(-korrutajasuurus,korrutajasuurus)
    tehe = "(" + sulutehe + ")" + "*" + negsulud(korrutaja)
    vastus = sulusumma*korrutaja
    return tehe,vastus


def jagamine(suurus,miinimum):
    x = 7
    y = 4
    while y == 0 or x%y != 0 or abs(x)<miinimum or abs(y)<miinimum or y==x:
        x = randint(-suurus,suurus)
        y = randint(-suurus,suurus)
    tehe = str(x) + "/" + negsulud(y)
    vastus = x/y
    return (tehe,vastus)

    
def ülesanne(tase):
    if tase == 1:####TASE 1 - LIITMINE 10 PIIRES
        tehe,vastus = liitminelahutamine(1,9,0)
    if tase == 2:####TASE 2 - LIITMINE/LAHUTAMINE 25 PIIRES
        tehe,vastus = liitminelahutamine(-25,25,7)
    if tase == 3:####TASE 3 - LIITMINE/LAHUTAMINE 300 PIIRES VÕI KORRUTAMINE 10 PIIRES
        coin = randint(0,1)#valib kas liitmine või korrutamine
        if coin == 0:
            tehe,vastus = liitminelahutamine(-150,150,50)
        if coin == 1:
            tehe,vastus = korrutamine(-9,9,5)
    if tase == 4: ####TASE 4 - LIITMINE/LAHUTAMINE 1000 PIIRES VÕI KORRUTAMINE 100 PIIRES
        coin = randint(0,2)
        if coin == 0:
            tehe,vastus = liitminelahutamine(-500,500,50)
        if coin == 1:
            tehe,vastus = korrutamine(-10,10,5)
        if coin == 2:
            tehe,vastus = jagamine(100,10)
    if tase == 5: ####TASE 5 - LIITMINE KÜMNENDMURDUDEGA 10 PIIRES VÕI LIIDAKORRUTA
        coin = randint(0,1)
        if coin == 0:
            tehe,vastus = liidaKymnendmurd(0, 10)
        else:
            tehe,vastus = liidakorruta(200,50,7,2)
    if tase == 6: ####TASE 6 - LAHUTAMINE KÜMNENDMURDUDEGA 10 PIIRES VÕI KORRUTALIIDA
        coin = randint(0,1)
        if coin == 0:
            tehe,vastus = lahutaKymnendmurd(0, 10)
        else:
            tehe,vastus = korrutaliida(15,5,250,125)
    return (tehe,vastus)
