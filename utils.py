from tkinter import *
import os
import stat

# nimesisestus
def tervitus():
    tervitus = Tk()
    tervitus.title("Nime sisestus")
    Label(tervitus, text="Palun sisesta nimi:").pack()
    nimi = Entry(tervitus)
    nimi.pack()
    nimi.focus_set()
    sisestaNimi = Button(tervitus, text = "Sisesta", width=20, command = lambda:handleButtonClick(tervitus, nimi))
    sisestaNimi.pack()
    tervitus.lift()
    tervitus.mainloop()

def handleButtonClick(tervitus, nimi):
    global saveName
    saveName = nimi.get()
    tervitus.destroy()

# salvestab tulemuse faili
def saveResultToFile(punkte):
    resultsF = open("tulemused.txt", "a")
    if os.stat("tulemused.txt")[stat.ST_SIZE] != 0:
        resultsF.write("\n")
    resultsF.write(saveName + ":" + str(punkte))
    resultsF.close()

# reastab parimad tulemused
def getBestResults():
    dictNames = {}
    dictResults = {}
    n = 0
    resultsF = open("tulemused.txt")

    for i in resultsF:
        resultReadSplit = i.split(":")
        dictNames[n] = resultReadSplit[0]        
        dictResults[n] = int(resultReadSplit[1].strip())
        n = n + 1
    resultsF.close()
        
    sortedKeys = sorted(dictResults, key = dictResults.get, reverse = True)
    bestResults = ""

    for count,i in enumerate(sortedKeys):
        # tagastab resultid varem, kui 10 elementi juba sisse loetud
        if(count > 9):
            return bestResults
        bestResults += dictNames.get(i) + " - " + str(dictResults.get(i)) + "\n"
    return bestResults

# tulemuste aken
def resultWindow():
    resultWindow = Tk()
    resultWindow.title("Parimad tulemused")
    resultWindow.geometry("250x250")
    tekstifont = font.Font(family='Helvetica', size=10, weight='normal')

    resultLabel = Label(resultWindow, text = getBestResults(), font = tekstifont)
    resultLabel.place(x = 85, y = 30)
